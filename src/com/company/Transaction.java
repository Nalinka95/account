package com.company;

public class Transaction {
    Account myAccount;

    public Transaction(Account myAccount) {
        this.myAccount = myAccount;
    }

    public synchronized boolean deposit(double depositingAmount){
        double currentAccountBalance=myAccount.getAccountBalance()+depositingAmount;
        myAccount.setAccountBalance(currentAccountBalance);
        return true;
    }
    public synchronized boolean withdrawal(double withdrawalAmount){
        double currentAmount=myAccount.getAccountBalance()-withdrawalAmount;
        if(currentAmount>=1000){
            myAccount.setAccountBalance(currentAmount);
            return true;
        }
        else {
            return false;
        }
    }

}
