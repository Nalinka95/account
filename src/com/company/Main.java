package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Account myAccount= new Account(20000,12312);
        myAccount.deposit(4000);
        myAccount.withdraw(2000);
        myAccount.deposit(1000);
        myAccount.withdraw(1500);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(myAccount.getAccountBalance());
    }
}
