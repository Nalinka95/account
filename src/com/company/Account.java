package com.company;

public class Account implements Runnable {
    private double accountBalance;
    private int accountNumber;
    private boolean isDeposit;
    Transaction customerTransaction;
    private double depositingAmount;
    private double withdrawalAmount;

    public Account(double accountBalance, int accountNumber) {
        setAccountBalance(accountBalance);
        setAccountNumber(accountNumber);
        customerTransaction = new Transaction(this);
    }

    @Override
    public void run() {
        if (isDeposit) {
            customerTransaction.deposit(depositingAmount);
            System.out.println("deposited Rs."+depositingAmount+" /=");
        } else {
            boolean isSuccessful=customerTransaction.withdrawal(withdrawalAmount);
            if(isSuccessful){
                System.out.println("withdrawn Rs."+withdrawalAmount+"/=");
            }
            else {
                System.out.println("withdrawn is not successful ..!!");
            }
        }
    }

    public boolean deposit(double depositingAmount) {
        this.depositingAmount = depositingAmount;
        isDeposit = true;
        Thread depositThread = new Thread(this);
        depositThread.start();
        return true;

    }

    public boolean withdraw(double withdrawalAmount) {
        isDeposit = false;
        this.withdrawalAmount = withdrawalAmount;
        Thread withdrawThread = new Thread(this);
        withdrawThread.start();
        return true;
    }


    public double getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    private void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }


}
